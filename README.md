# Excavator Control Assignment
====================

### About
This is node-red workflow for Excavator Control Assignment with [excavator-simulator](https://gitlab.com/rwth-crmasters-sose22/course-material/excavator-simulator).

# Instructions
1. Install node-red
1. Enable project feature for node-red & restart node-red
1. Create project by clone this repo
1. Deploy
1. Open dashboard http://127.0.0.1:1880/ui
1. Open simulator from sidemenu (top left button ☰ ⮕ ↑Simulator)
1. Copy the session id (the string below the top right green bar 🟩 ) and paste to the Session input.
1. Drag the slider q0-q5. Have fun!
## Screenshots
- Dashboard
![](/assets/excavator_control_dashboard_wei_yang.webp)

- Flow Overview
![](/assets/excavator_control_wei_yang.webp)

- Sidemenu to open Simulator
![](/assets/excavator_control_dashboard_sidemenu_wei_yang.webp)
